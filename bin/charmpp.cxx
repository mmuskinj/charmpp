#include "TString.h"

#include "WCharmLoop.h"

int main(int argc, char **argv) {

    // file name
    TString file_name = argv[1];

    // out path
    TString out_path = argv[2];

    // sum of weights
    double sum = atof(argv[3]);

    Charm::WCharmLoop *el = new Charm::WCharmLoop(file_name, out_path, sum);

    el->run();
}
