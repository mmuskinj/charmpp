#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)

import sys, os

path = os.environ['CHARMPP_NTUPLE_PATH']

datasets = {}

# r=root, d=directories, f=files
for r, d, f in os.walk(path):
    for file in f:
        if '.root' in file:
            sample = os.path.basename(r).split(".")[2]
            if not sample in datasets.keys():
                datasets[sample] = []

            datasets[sample] += [os.path.join(r, file)]

subdatasets = {}

for sample in datasets:
    subdatasets[sample] = {}

    for f in datasets[sample]:
        dsid = os.path.basename(f).split(".")[2]
        short = os.path.basename(f).split(".")[3]

        if not dsid in subdatasets[sample].keys():
            subdatasets[sample][dsid] = []

        subdatasets[sample][dsid] += [f]

jobs_file = open(os.path.join(os.environ['CHARMPP_RUN_PATH'], 'jobs.txt'), 'w')

for sample, dataset in subdatasets.iteritems():
    print ("--- %s ---" % sample)
    sample_out_path = os.path.join(os.environ['CHARMPP_RUN_PATH'], sample)
    if not os.path.isdir(sample_out_path):
        os.makedirs(sample_out_path)
    if not 'data' in sample:
        for d in dataset:
            initial_sum_of_weights = 0
            for f in subdatasets[sample][d]:
                file = ROOT.TFile(f, "READ")
                for key in [x.GetName() for x in file.GetListOfKeys()]:
                    if "CutBookkeeper" in key:
                        initial_sum_of_weights += file.Get(key).GetBinContent(2)
                file.Close()
            print ("dsid %s had %s sub-files: %s" %
                (d, len(subdatasets[sample][d]), initial_sum_of_weights) )
            jobs_file.write("%s\t%s\t%s\t%s\n" % (d, os.path.dirname(subdatasets[sample][d][0]), sample_out_path, initial_sum_of_weights) )
    else:
        print ("%s sub-files" % len(datasets[sample]) )
        jobs_file.write("%s\t%s\t%s\t%s\n" % (sample, os.path.dirname(datasets[sample][0]), sample_out_path, -1) )

jobs_file.close()


