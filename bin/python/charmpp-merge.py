#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)

import os, sys

merge_path = os.environ['CHARMPP_RUN_PATH']

jobs_file = os.path.join(os.environ['CHARMPP_RUN_PATH'], 'jobs.txt')
samples = []
with open(jobs_file, 'r') as f:
    for l in f:
        sample = l.split()[2].split("/")[-1]
        if not sample in samples:
            samples += [sample]

data = []

import subprocess
for s in samples:

    if 'data' in s:
        data += ["%s.root" % s]

    path = os.path.join(os.environ['CHARMPP_RUN_PATH'], s)

    files = [os.path.join(os.environ['CHARMPP_RUN_PATH'], s, f)
        for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

    p = subprocess.Popen(["hadd", "-f", "%s.root" % s] + files)
    p.communicate()

if data:
    p = subprocess.Popen(["hadd", "-f", "data.root"] + data)
    p.communicate()
