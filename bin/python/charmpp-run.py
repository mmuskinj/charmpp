#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)

import sys, os
import subprocess
import psutil

print ("will run with %s threads on %s cores" % (
    psutil.cpu_count(logical = True), psutil.cpu_count(logical = False) ) )

def launch_wrapper(args):
    FNULL = open(os.devnull, 'w')
    p = subprocess.Popen(["charmpp", args[0], args[1], args[2]],
        stdout=FNULL, stderr=subprocess.STDOUT)
    p.communicate()
    return

jobs_file = os.path.join(os.environ['CHARMPP_RUN_PATH'], 'jobs.txt')
if not os.path.isfile(jobs_file):
    print ("no jobs.txt file found in %s" % os.environ['CHARMPP_RUN_PATH'])
    print ("call charmpp-prepare first")
    sys.exit(1000)


jobs = []
with open(jobs_file, 'r') as f:
    for l in f:
        jobs += [l]


for j in jobs:
    job = j.split()

    print ("--- running sub-jobs for dsid %s ---" % job[0])

    process = False

    if len(sys.argv) > 1:
        for arg in sys.argv[1:]:
            if arg in job[0] or arg in job[1]:
                process = True
    else:
        process = True

    if not process:
        continue


    concurrnet_jobs = []
    # r=root, d=directories, f=files
    for r, d, f in os.walk(job[1]):
        for file in f:
            if job[0] in file or 'data' in job[0]:
                concurrnet_jobs += [[os.path.join(r, file), job[2], job[-1]]] 

    from multiprocessing import Pool
    import tqdm
    p = Pool(psutil.cpu_count(logical = True))
    for _ in tqdm.tqdm(p.imap_unordered(launch_wrapper, concurrnet_jobs), total=len(concurrnet_jobs)):
        pass

