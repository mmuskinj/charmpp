#!/bin/bash

export CHARMPP_BUILD_PATH=/afs/f9.ijs.si/home/miham/analysis/build/charmpp

export CHARMPP_RUN_PATH=/afs/f9.ijs.si/home/miham/analysis/run/charmpp

export CHARMPP_NTUPLE_PATH=/ceph/grid/home/atlas/miham/CharmAnalysis/v1_inc

mkdir -p $CHARMPP_BUILD_PATH
mkdir -p $CHARMPP_RUN_PATH

export PATH=${CHARMPP_BUILD_PATH}/bin:$PATH
