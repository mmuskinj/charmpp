#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)

import os

# ATLAS Style
atlasrootstyle = os.path.join(os.environ["CHARMPP_BUILD_PATH"], "atlasrootstyle")
ROOT.gROOT.LoadMacro(os.path.join(atlasrootstyle, "AtlasStyle.C"))
ROOT.gROOT.LoadMacro(os.path.join(atlasrootstyle, "AtlasLabels.C"))
ROOT.gROOT.LoadMacro(os.path.join(atlasrootstyle, "AtlasUtils.C"))
ROOT.SetAtlasStyle()

out_path = os.path.join(os.environ["CHARMPP_RUN_PATH"])
plot_path = os.path.join(out_path, "plots")
if not os.path.isdir(plot_path):
    os.makedirs(plot_path)

data = ROOT.TFile(os.path.join(out_path, "data.root"), "READ")
wjets = ROOT.TFile(os.path.join(out_path, "Wjets_Powheg.root"), "READ")
top = ROOT.TFile(os.path.join(out_path, "Top.root"), "READ")

mc = [wjets, top]

channels = []
histograms = []

for key in data.GetListOfKeys():
    name = key.GetName()
    channel = name.split("_")[0]
    if not channel in channels:
        channels += [channel]

    histogram = name.replace(channel+"_", "")
    if not histogram in histograms:
        histograms += [histogram]

print (" ".join(channels))
print (" ".join(histograms))

from utils import ratioplot, getLegend, getStatErr, getStatErrRatio, getProxyHisto, getRatio

for c in channels:
    for h in histograms:

        name = "_".join([c, h])

        h_data = data.Get(name)
        h_mc = [f.Get(name) for f in mc]
        print h_mc
        hs_mc = ROOT.THStack()
        for x in h_mc:
            print x
            hs_mc.Add(x)

        leg = getLegend(4, x1 = 0.65, x2 = 0.9, y2 = 0.90, size = 0.065)
        leg.AddEntry(h_data, "#font[42]{Data}", "pe")
        leg.AddEntry(h_mc[0], "#font[42]{W+jets}", "f")
        leg.AddEntry(h_mc[1], "#font[42]{Top}", "f")

        canv, pad1, pad2 = ratioplot.getCanvas(h+c, True, True)
        pad1.cd()

        # total stat err
        total_stat_err = getStatErr(hs_mc.GetStack().Last())
        total_stat_err.SetFillColor(ROOT.kGray+2)
        total_stat_err.SetFillStyle(3345)
        total_stat_err.SetLineWidth(0)
        leg.AddEntry(total_stat_err, "#font[42]{MC Stat. Unc.}", "f")

        hs_mc.Draw("hist")
        hs_mc.SetMaximum(h_data.GetMaximum()*1.6)
        hs_mc.SetMinimum(1e-1)
        total_stat_err.Draw("E2")
        data.Draw("same pe")
        ROOT.ATLASLabel(0.18,0.85,"Internal",1, 0.065)
        ROOT.myText(0.18,0.85-0.065,1,"#sqrt{s} = 13 TeV, 36.1 fb^{-1}", 0.065)
        ROOT.myText(0.18,0.85-2*0.065,1,"%s channel" % (c), 0.065)
        leg.Draw()
        ratioplot.configureUpperPad(hs_mc, "Events")

        pad2.cd()
        pad2.SetFillStyle(4000)
        h_ratio = getRatio(h_data, hs_mc.GetStack().Last())
        total_stat_err_ratio = getStatErrRatio(hs_mc.GetStack().Last())
        total_stat_err_ratio.SetFillColor(ROOT.kGray+2)
        total_stat_err_ratio.SetFillStyle(3345)
        h_ratio.Draw("p e x0")
        total_stat_err_ratio.Draw("E2")
        ratioplot.configureLowerPad(h_ratio, 0.49, 1.50, h, "Ratio")

        canv.Print("plots/%s_%s.pdf" % (c, h))




