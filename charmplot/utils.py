import ROOT

def getProxyHisto(h):
    proxy = h.Clone(h.GetName()+"proxy")
    for i in range(1, h.GetNbinsX()+1):
        proxy.SetBinContent(i, 0)
        proxy.SetBinError(i, 0)
    return proxy

def getRatio(data, h):
    out = data.Clone(data.GetName()+"ratio")
    for i in range(1, h.GetNbinsX()+1):
        y = out.GetBinContent(i)
        ymc = h.GetBinContent(i)
        if ymc > 0:
            out.SetBinContent(i, y/ymc)
            out.SetBinError(i, out.GetBinError(i)/ymc)
        else:
            out.SetBinContent(i, 0)
            out.SetBinError(i, 0)
    return out

def getStatErr(h):
    gr = ROOT.TGraphErrors()
    for i in range(1, h.GetNbinsX()+1):
        x = h.GetBinCenter(i)
        y = h.GetBinContent(i)
        gr.SetPoint(gr.GetN(), x, y)
        gr.SetPointError(gr.GetN()-1, (h.GetBinWidth(i)/2.), h.GetBinError(i))
    return gr

def getStatErrRatio(h):
    gr = ROOT.TGraphErrors()
    for i in range(1, h.GetNbinsX()+1):
        x = h.GetBinCenter(i)
        y = h.GetBinContent(i)
        gr.SetPoint(gr.GetN(), x, 1.0)
        if y != 0:
            gr.SetPointError(gr.GetN()-1, (h.GetBinWidth(i)/2.), h.GetBinError(i)/y)
        else:
            gr.SetPointError(gr.GetN()-1, (h.GetBinWidth(i)/2.), 0)
    return gr

def getLegend(N, x1 = 0.6, x2 = 0.9, y2 = 0.9, size = 0.045):
    leg = ROOT.TLegend(x1, y2 - N*(size), x2, y2)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextSize(size)
    return leg

def pdfEnding(i, last = False):
    if i == 1:
        return "("
    elif last:
        return ")"
    else:
        return ""

class RatioPlot:
    rsplit = 0.35
    pad1TopMarin = 0.07
    pad1BotMarin = 0.04
    pad2TopMarin = 0.06
    pad2BotMarin = 0.4
    LeftMargin = 0.15

    def getCanvas(self, uniqueName, nologx=False, nology=False, rsplit = 0.35):
        self.rsplit = rsplit
        canv = ROOT.TCanvas(uniqueName, uniqueName, 800, 600)
        pad1 = ROOT.TPad("pad1"+uniqueName, "top pad" +
                         uniqueName, 0., self.rsplit, 1., 1.)
        pad1.SetTopMargin(self.pad1TopMarin)
        pad1.SetBottomMargin(self.pad1BotMarin)
        pad1.SetLeftMargin(self.LeftMargin)
        if not nologx:
            pad1.SetLogx()
        if not nology:
            pad1.SetLogy()
        pad1.Draw()
        pad2 = ROOT.TPad("pad2"+uniqueName, "bottom pad" +
                         uniqueName, 0, 0, 1, self.rsplit+0.05)
        pad2.SetTopMargin(self.pad2TopMarin)
        pad2.SetBottomMargin(self.pad2BotMarin)
        pad2.SetLeftMargin(self.LeftMargin)
        pad2.SetTicky()
        pad2.SetTickx()
        if not nologx:
            pad2.SetLogx()
        pad2.SetGridy()
        pad2.Draw()
        pad2.SetFillStyle(0)
        pad2.SetFillColor(0)
        return canv, pad1, pad2

    def configureUpperPad(self, hs1, yaxis=""):
        hs1.GetXaxis().SetLabelSize(0)
        hs1.GetYaxis().SetLabelSize(0.062)
        hs1.GetYaxis().SetTitleSize(0.065)
        hs1.GetYaxis().SetTitleOffset(1.1)
        hs1.GetYaxis().SetTitle(self.yaxis if yaxis == "" else yaxis)
        ROOT.gPad.RedrawAxis()

    def configureLowerPad(self, h2, ratioDn, ratioUp, xaxis, yaxisr):
        h2.GetXaxis().SetTitle(xaxis)
        h2.GetYaxis().SetTitle(yaxisr)
        h2.GetXaxis().SetTitleSize(0.11 * (0.35/self.rsplit))
        h2.GetXaxis().SetTitleOffset(1.2 * (0.35/self.rsplit))
        h2.GetXaxis().SetLabelSize(0.10 * (0.35/self.rsplit))
        h2.GetYaxis().SetTitleSize(0.11 * (0.35/self.rsplit))
        h2.GetYaxis().SetTitleOffset(0.65 * (0.35/self.rsplit))
        h2.GetYaxis().SetLabelSize(0.10 * (0.35/self.rsplit))
        h2.GetYaxis().SetLabelOffset(0.005 * (0.35/self.rsplit))
        h2.GetYaxis().SetNdivisions(6)
        if ratioDn > 0:
            h2.SetMinimum(1.0 - ratioDn)
            h2.SetMaximum(1.0 + ratioUp)
        ROOT.gPad.RedrawAxis()

ratioplot = RatioPlot()
