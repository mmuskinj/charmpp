#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H

#include "TString.h"

namespace Charm {

TString get_run_number_string(bool is_mc);

std::string basename(std::string filePath, bool withExtension = true, char seperator = '/');

float get_cross_section(int dsid);

} // namespace Charm

#endif // HELPER_FUNCTIONS_H
