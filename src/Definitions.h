namespace Charm {

    static const double ELECTRON_MASS = 0.5109989461;
    static const double MUON_MASS = 105.6583745;

    static const double LUMI_2015 = 3219.56;
    static const double LUMI_2016 = 32988.1;

} // namespace Charm
