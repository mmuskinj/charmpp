#include <iostream>

#include "TH1F.h"

#include "EventLoopBase.h"
#include "HelperFunctions.h"

namespace Charm {

void EventLoopBase::run() {

    // read meta data
    this->read_meta_data();

    // initialize
    this->initialize();

    // loop over all events
    for (; m_current_event < m_num_events; m_current_event++) {

        // read branches
        for (auto *br : m_branches) {
            if (br->GetEntry(m_current_event) <= 0) {
                return;
            }
        }

        // execute
        if (this->execute() < 0)
            return;
    }

    // finalize
    this->finalize();
}

void EventLoopBase::read_meta_data() {}

void EventLoopBase::initialize() {}

int EventLoopBase::execute() { return -1; }

void EventLoopBase::finalize() {
    TFile out_file(m_out_path + "/" + m_output_file_name, "RECREATE");
    out_file.cd();
    for (const auto& kv : m_histograms) {
        kv.second->Write();
    }
    out_file.Close();
}

void EventLoopBase::add_branch(TString name) {
    TBranch *br = dynamic_cast<TBranch *>(m_tree->GetBranch(name));
    if (br) {
        m_branches.push_back(br);
    }
}

void EventLoopBase::fill_histogram(TString name, float val) {
    m_histograms[m_channel + "_" + name]->Fill(val, m_event_weight);
}

void EventLoopBase::add_histogram(TString name, int n, float xmin, float xmax) {
    m_histograms[name] = new TH1F(name, name, n, xmin, xmax);
    m_histograms[name]->Sumw2(true);
}

EventLoopBase::EventLoopBase(TString input_file, TString out_path, double init_sum_of_w, TString tree_name)
    : m_tree_name(tree_name),
      m_current_event(0),
      m_out_path(out_path),
      m_init_sum_of_w(init_sum_of_w) {
    // read root file
    m_input_file = new TFile(input_file);

    // read tree
    m_tree = dynamic_cast<TTree *>(m_input_file->Get(m_tree_name));

    // number of events
    m_num_events = m_tree->GetEntries();

    // output file name
    m_output_file_name = Charm::basename(std::string(input_file));

    std::cout << "Read " << m_num_events << " events" << std::endl;
}

} // namespace Charm
