#include <iostream>
#include <math.h>

#include "TRegexp.h"
#include "TVector2.h"

#include "HelperFunctions.h"
#include "WCharmLoop.h"

namespace Charm {

void WCharmLoop::read_meta_data() {

    std::cout << "read_meta_data" << std::endl;

    for (auto *key : *m_input_file->GetListOfKeys()) {
        TString obj_name = key->GetName();

        if (obj_name.BeginsWith("CutBookkeeper")) {
            std::cout << obj_name << std::endl;

            // MC
            m_is_mc = true;

            // extract DSID
            m_dataset_id = TString(obj_name(TRegexp("[0-9]+")).Data()).Atoi();

            std::cout << "m_dataset_id " << m_dataset_id << std::endl;

            m_cross_section = Charm::get_cross_section(m_dataset_id);

            std::cout << "m_cross_section " << m_cross_section << std::endl;
            std::cout << "m_init_sum_of_w " << m_init_sum_of_w << std::endl;
        }
    }
}

void WCharmLoop::initialize() {

    std::cout << "initialize" << std::endl;

    m_lumi = Charm::LUMI_2015 + Charm::LUMI_2016;

    connect_branches();

    initialize_histograms();

}

int WCharmLoop::execute() {

    // b-jet veto
    if (!get_jets())
        return 1;

    construct_met();

    get_electrons();

    get_muons();

    // require exactly one lepton
    if ( (m_electrons.size() + m_muons.size()) != 1 )
        return 1;

    m_event_weight = 1.;

    // electron or muon channel
    if (m_electrons.size() == 1) {
        m_lep = m_electrons.at(0).first;
        if (m_is_mc)
            m_event_weight *= m_electrons.at(0).second;
        m_channel = "el";
    }
    else if (m_muons.size() == 1) {
        m_lep = m_muons.at(0).first;
        if (m_is_mc)
            m_event_weight *= m_muons.at(0).second;
        m_channel = "mu";
    }
    else {
        std::cout << "wrong size" << std::endl;
        return -1;
    }

    // transverse mass
    m_met_dphi = TVector2::Phi_mpi_pi(m_met_phi - m_lep.Phi());
    m_met_mt = sqrt(2*m_lep.Pt()*(m_met)*(1-cos(m_met_dphi)));

    if (m_met < 25000. || m_met_mt < 50000.)
        return 1;

    // event weight
    if (m_is_mc) {
        m_event_weight *= (m_lumi*m_cross_section/m_init_sum_of_w)*
                          m_EventInfo_NOSYS_PileupWeight*
                          m_EventInfo_jvt_effSF_NOSYS*
                          m_EventInfo_fjvt_effSF_NOSYS*
                          m_EventInfo_mcEventWeights->at(0);
    }

    // fill histograms
    fill_histograms();

    return 1;
}

bool WCharmLoop::get_jets() {
    m_jets.clear();
    for (unsigned int i = 0; i < m_AnalysisJetsFinal_NOSYS_m->size(); i++) {
        if (!(m_AnalysisJetsFinal_NOSYS_pt->at(i) > 30000.))
            continue;
        if (m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_77->at(i))
            return false;

        TLorentzVector jet;
        jet.SetPtEtaPhiM(m_AnalysisJetsFinal_NOSYS_pt->at(i),
                         m_AnalysisJetsFinal_eta->at(i),
                         m_AnalysisJetsFinal_phi->at(i),
                         m_AnalysisJetsFinal_NOSYS_m->at(i));

        std::pair<TLorentzVector, float> pair(jet, 1.0);
        m_jets.push_back(pair);
    }

    return true;
}

void WCharmLoop::get_electrons() {
    m_electrons.clear();
    for (unsigned int i = 0; i < m_AnalysisElectronsOR_NOSYS_pt->size(); i++) {
        if (!(m_AnalysisElectronsOR_NOSYS_pt->at(i) > 30000.))
            continue;
        if (!m_AnalysisElectronsOR_likelihood_Tight->at(i))
            continue;
        if (!m_AnalysisElectronsOR_NOSYS_isIsolated_FCTight->at(i))
            continue;
        if (!electron_is_trigger_matched(i))
            continue;

        TLorentzVector el;
        el.SetPtEtaPhiM(m_AnalysisElectronsOR_NOSYS_pt->at(i),
                        m_AnalysisElectronsOR_eta->at(i),
                        m_AnalysisElectronsOR_phi->at(i),
                        Charm::ELECTRON_MASS);

        float sf = 1.;
        if (m_is_mc) {
            sf *= m_AnalysisElectronsOR_effSF_NOSYS->at(i) *
                  m_AnalysisElectronsOR_effSF_ID_Tight_NOSYS->at(i) *
                  m_AnalysisElectronsOR_effSF_Isol_Tight_FCTight_NOSYS->at(i) *
                  m_AnalysisElectronsOR_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS->at(i);
        }

        std::pair<TLorentzVector, float> pair(el, sf);
        m_electrons.push_back(pair);
    }
}

void WCharmLoop::get_muons() {
    m_muons.clear();
    for (unsigned int i = 0; i < m_AnalysisMuonsOR_NOSYS_pt->size(); i++) {
        if (!(m_AnalysisMuonsOR_NOSYS_pt->at(i) > 30000.))
            continue;
        if (!m_AnalysisMuonsOR_NOSYS_isIsolated_FCTight_FixedRad->at(i))
            continue;
        if (!muon_is_trigger_matched(i))
            continue;

        TLorentzVector mu;
        mu.SetPtEtaPhiM(m_AnalysisMuonsOR_NOSYS_pt->at(i),
                        m_AnalysisMuonsOR_eta->at(i),
                        m_AnalysisMuonsOR_phi->at(i),
                        Charm::MUON_MASS);

        float sf = 1.;
        if (m_is_mc) {
            sf = m_AnalysisMuonsOR_muon_effSF_NOSYS->at(i) *
                 m_AnalysisMuonsOR_muon_effSF_TTVA_NOSYS->at(i) *
                 m_AnalysisMuonsOR_muon_effSF_Isol_FCTight_FixedRad_NOSYS->at(i);

            if (m_EventInfo_runNumber < 290000) {
                sf *= m_AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS->at(i) /
                    m_AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS->at(i);
            } else {
                sf *= m_AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS->at(i) /
                    m_AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS->at(i);
            }
        }

        std::pair<TLorentzVector, float> pair(mu, sf);
        m_muons.push_back(pair);
    }
}

bool WCharmLoop::muon_is_trigger_matched(unsigned int i) {
    if (m_EventInfo_runNumber < 290000) {
        return m_AnalysisMuonsOR_matched_HLT_mu50->at(i) ||
               m_AnalysisMuonsOR_matched_HLT_mu20_iloose_L1MU15->at(i);
    } else {
        return m_AnalysisMuonsOR_matched_HLT_mu50->at(i) ||
               m_AnalysisMuonsOR_matched_HLT_mu26_ivarmedium->at(i);
    }
}

bool WCharmLoop::electron_is_trigger_matched(unsigned int i) {
    if (m_EventInfo_runNumber < 290000) {
        return m_AnalysisElectronsOR_matched_HLT_e24_lhmedium_L1EM20VH->at(i) ||
               m_AnalysisElectronsOR_matched_HLT_e60_lhmedium->at(i) ||
               m_AnalysisElectronsOR_matched_HLT_e120_lhloose->at(i);
    } else {
        return m_AnalysisElectronsOR_matched_HLT_e26_lhtight_nod0_ivarloose->at(i) ||
               m_AnalysisElectronsOR_matched_HLT_e60_lhmedium_nod0->at(i) ||
               m_AnalysisElectronsOR_matched_HLT_e140_lhloose_nod0->at(i);
    }
}

void WCharmLoop::construct_met() {
    for (unsigned int i = 0; i < m_AnalysisMET_name->size(); i++) {
        std::string name = m_AnalysisMET_name->at(i);
        if (name == "Final") {
            m_met = sqrt(m_AnalysisMET_NOSYS_mpx->at(i)*m_AnalysisMET_NOSYS_mpx->at(i) +
                         m_AnalysisMET_NOSYS_mpy->at(i)*m_AnalysisMET_NOSYS_mpy->at(i));
            m_met_phi = atan2(m_AnalysisMET_NOSYS_mpy->at(i), m_AnalysisMET_NOSYS_mpx->at(i));
            return;
        }
    }
}

void WCharmLoop::fill_histograms() {
    fill_histogram("lep_pt", m_lep.Pt()/1000.);
    fill_histogram("lep_eta", m_lep.Eta());
    fill_histogram("lep_phi", m_lep.Phi());
    fill_histogram("met_mt", m_met_mt/1000.);
    fill_histogram("met_met", m_met/1000.);
    fill_histogram("met_dphi", m_met_dphi);
    fill_histogram("njets", m_jets.size());
}

void WCharmLoop::initialize_histograms() {
    std::vector<TString> channels({"el", "mu"});
    for (auto& channel : channels) {
        add_histogram(channel + "_" + "lep_pt", 100, 0, 200);
        add_histogram(channel + "_" + "lep_eta", 100, -3.0, 3.0);
        add_histogram(channel + "_" + "lep_phi", 100, -M_PI, M_PI);
        add_histogram(channel + "_" + "met_mt", 100, 0, 200);
        add_histogram(channel + "_" + "met_met", 100, 0, 200);
        add_histogram(channel + "_" + "met_dphi", 100, -M_PI, M_PI);
        add_histogram(channel + "_" + "njets", 20, -0.5, 19.5);
    }
}

void WCharmLoop::connect_branches() {
    TString name;

    // scalars
    name = Charm::get_run_number_string(m_is_mc);
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_runNumber);

    name = "EventInfo_correctedScaled_averageInteractionsPerCrossing";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_correctedScaled_averageInteractionsPerCrossing);

    name = "EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH);

    name = "EventInfo_trigPassed_HLT_e60_lhmedium";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_trigPassed_HLT_e60_lhmedium);

    name = "EventInfo_trigPassed_HLT_e120_lhloose";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_trigPassed_HLT_e120_lhloose);

    name = "EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose);

    name = "EventInfo_trigPassed_HLT_e60_lhmedium_nod0";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0);

    name = "EventInfo_trigPassed_HLT_e140_lhloose_nod0";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_trigPassed_HLT_e140_lhloose_nod0);

    name = "EventInfo_trigPassed_HLT_mu20_iloose_L1MU15";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15);

    name = "EventInfo_trigPassed_HLT_mu26_ivarmedium";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_trigPassed_HLT_mu26_ivarmedium);

    name = "EventInfo_trigPassed_HLT_mu50";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_EventInfo_trigPassed_HLT_mu50);

    // vectors    
    name = "AnalysisJetsFinal_NOSYS_m";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisJetsFinal_NOSYS_m);

    name = "AnalysisJetsFinal_NOSYS_pt";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisJetsFinal_NOSYS_pt);

    name = "AnalysisJetsFinal_eta";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisJetsFinal_eta);

    name = "AnalysisJetsFinal_phi";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisJetsFinal_phi);

    name = "AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_77";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_77);

    name = "AnalysisMuonsOR_NOSYS_pt";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_NOSYS_pt);

    name = "AnalysisMuonsOR_eta";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_eta);

    name = "AnalysisMuonsOR_phi";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_phi);

    name = "AnalysisMuonsOR_charge";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_charge);

    name = "AnalysisMuonsOR_NOSYS_isIsolated_FCTight_FixedRad";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_NOSYS_isIsolated_FCTight_FixedRad);

    name = "AnalysisMuonsOR_matched_HLT_mu20_iloose_L1MU15";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_matched_HLT_mu20_iloose_L1MU15);

    name = "AnalysisMuonsOR_matched_HLT_mu26_ivarmedium";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_matched_HLT_mu26_ivarmedium);

    name = "AnalysisMuonsOR_matched_HLT_mu50";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_matched_HLT_mu50);

    name = "AnalysisElectronsOR_NOSYS_pt";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_NOSYS_pt);

    name = "AnalysisElectronsOR_eta";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_eta);

    name = "AnalysisElectronsOR_phi";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_phi);

    name = "AnalysisElectronsOR_charge";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_charge);

    name = "AnalysisElectronsOR_likelihood_Medium";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_likelihood_Medium);

    name = "AnalysisElectronsOR_likelihood_Tight";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_likelihood_Tight);

    name = "AnalysisElectronsOR_NOSYS_isIsolated_FCHighPtCaloOnly";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_NOSYS_isIsolated_FCHighPtCaloOnly);

    name = "AnalysisElectronsOR_NOSYS_isIsolated_FCLoose";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_NOSYS_isIsolated_FCLoose);

    name = "AnalysisElectronsOR_NOSYS_isIsolated_FCTight";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_NOSYS_isIsolated_FCTight);

    name = "AnalysisElectronsOR_NOSYS_isIsolated_Gradient";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_NOSYS_isIsolated_Gradient);

    name = "AnalysisElectronsOR_matched_HLT_e24_lhmedium_L1EM20VH";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_matched_HLT_e24_lhmedium_L1EM20VH);

    name = "AnalysisElectronsOR_matched_HLT_e60_lhmedium";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_matched_HLT_e60_lhmedium);

    name = "AnalysisElectronsOR_matched_HLT_e120_lhloose";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_matched_HLT_e120_lhloose);

    name = "AnalysisElectronsOR_matched_HLT_e26_lhtight_nod0_ivarloose";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_matched_HLT_e26_lhtight_nod0_ivarloose);

    name = "AnalysisElectronsOR_matched_HLT_e60_lhmedium_nod0";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_matched_HLT_e60_lhmedium_nod0);

    name = "AnalysisElectronsOR_matched_HLT_e140_lhloose_nod0";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_matched_HLT_e140_lhloose_nod0);

    name = "AnalysisMET_name";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMET_name);

    name = "AnalysisMET_NOSYS_mpx";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMET_NOSYS_mpx);

    name = "AnalysisMET_NOSYS_mpy";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMET_NOSYS_mpy);

    name = "AnalysisMET_NOSYS_sumet";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMET_NOSYS_sumet);

    name = "AnalysisMET_NOSYS_significance";
    add_branch(name);
    m_tree->SetBranchAddress(name, &m_AnalysisMET_NOSYS_significance);

    if (m_is_mc) {
        name = "EventInfo_mcEventWeights";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_EventInfo_mcEventWeights);

        name = "EventInfo_NOSYS_PileupWeight";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_EventInfo_NOSYS_PileupWeight);

        name = "EventInfo_jvt_effSF_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_EventInfo_jvt_effSF_NOSYS);

        name = "EventInfo_fjvt_effSF_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_EventInfo_fjvt_effSF_NOSYS);

        name = "AnalysisElectronsOR_effSF_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_effSF_NOSYS);

        name = "AnalysisElectronsOR_effSF_ID_Tight_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_effSF_ID_Tight_NOSYS);

        name = "AnalysisElectronsOR_effSF_Isol_Tight_FCTight_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_effSF_Isol_Tight_FCTight_NOSYS);

        name = "AnalysisElectronsOR_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisElectronsOR_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS);

        name = "AnalysisMuonsOR_muon_effSF_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_muon_effSF_NOSYS);

        name = "AnalysisMuonsOR_muon_effSF_TTVA_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_muon_effSF_TTVA_NOSYS);

        name = "AnalysisMuonsOR_muon_effSF_Isol_FCTight_FixedRad_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_muon_effSF_Isol_FCTight_FixedRad_NOSYS);

        name = "AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);

        name = "AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS);

        name = "AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);

        name = "AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS";
        add_branch(name);
        m_tree->SetBranchAddress(name, &m_AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS);

    }

}

WCharmLoop::WCharmLoop(TString input_file, TString out_path, double init_sum_of_w, TString tree_name)
    : EventLoopBase(input_file, out_path, init_sum_of_w, tree_name),
      m_is_mc(false),
      m_dataset_id(-1),
      m_EventInfo_runNumber(),
      m_EventInfo_correctedScaled_averageInteractionsPerCrossing(),
      m_EventInfo_NOSYS_PileupWeight(),
      m_EventInfo_jvt_effSF_NOSYS(),
      m_EventInfo_fjvt_effSF_NOSYS(),
      m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH(),
      m_EventInfo_mcEventWeights(),
      m_EventInfo_trigPassed_HLT_e60_lhmedium(),
      m_EventInfo_trigPassed_HLT_e120_lhloose(),
      m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose(),
      m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0(),
      m_EventInfo_trigPassed_HLT_e140_lhloose_nod0(),
      m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15(),
      m_EventInfo_trigPassed_HLT_mu26_ivarmedium(),
      m_EventInfo_trigPassed_HLT_mu50(),
      m_AnalysisJetsFinal_NOSYS_m(),
      m_AnalysisJetsFinal_NOSYS_pt(),
      m_AnalysisJetsFinal_eta(),
      m_AnalysisJetsFinal_phi(),
      m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_60(),
      m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_70(),
      m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_77(),
      m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_85(),
      m_AnalysisMuonsOR_NOSYS_pt(),
      m_AnalysisMuonsOR_eta(),
      m_AnalysisMuonsOR_phi(),
      m_AnalysisMuonsOR_charge(),
      m_AnalysisMuonsOR_muon_effSF_NOSYS(),
      m_AnalysisMuonsOR_muon_effSF_TTVA_NOSYS(),
      m_AnalysisMuonsOR_muon_effSF_Isol_FCTightTrackOnly_FixedRad_NOSYS(),
      m_AnalysisMuonsOR_muon_effSF_Isol_FCLoose_FixedRad_NOSYS(),
      m_AnalysisMuonsOR_muon_effSF_Isol_FCTight_FixedRad_NOSYS(),
      m_AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS(),
      m_AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS(),
      m_AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS(),
      m_AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS(),
      m_AnalysisMuonsOR_NOSYS_isIsolated_FCTightTrackOnly_FixedRad(),
      m_AnalysisMuonsOR_NOSYS_isIsolated_FCLoose_FixedRad(),
      m_AnalysisMuonsOR_NOSYS_isIsolated_FCTight_FixedRad(),
      m_AnalysisMuonsOR_matched_HLT_mu20_iloose_L1MU15(),
      m_AnalysisMuonsOR_matched_HLT_mu26_ivarmedium(),
      m_AnalysisMuonsOR_matched_HLT_mu50(), m_AnalysisElectronsOR_NOSYS_pt(),
      m_AnalysisElectronsOR_eta(), m_AnalysisElectronsOR_phi(),
      m_AnalysisElectronsOR_charge(), m_AnalysisElectronsOR_effSF_NOSYS(),
      m_AnalysisElectronsOR_effSF_ID_Tight_NOSYS(),
      m_AnalysisElectronsOR_effSF_Isol_Tight_FCTight_NOSYS(),
      m_AnalysisElectronsOR_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS(),
      m_AnalysisElectronsOR_likelihood_Medium(),
      m_AnalysisElectronsOR_likelihood_Tight(),
      m_AnalysisElectronsOR_NOSYS_isIsolated_FCHighPtCaloOnly(),
      m_AnalysisElectronsOR_NOSYS_isIsolated_FCLoose(),
      m_AnalysisElectronsOR_NOSYS_isIsolated_FCTight(),
      m_AnalysisElectronsOR_NOSYS_isIsolated_Gradient(),
      m_AnalysisElectronsOR_matched_HLT_e24_lhmedium_L1EM20VH(),
      m_AnalysisElectronsOR_matched_HLT_e60_lhmedium(),
      m_AnalysisElectronsOR_matched_HLT_e120_lhloose(),
      m_AnalysisElectronsOR_matched_HLT_e26_lhtight_nod0_ivarloose(),
      m_AnalysisElectronsOR_matched_HLT_e60_lhmedium_nod0(),
      m_AnalysisElectronsOR_matched_HLT_e140_lhloose_nod0(),
      m_AnalysisMET_name(),
      m_AnalysisMET_NOSYS_mpx(),
      m_AnalysisMET_NOSYS_mpy(),
      m_AnalysisMET_NOSYS_sumet(),
      m_AnalysisMET_NOSYS_significance() {}


} // namespace Charm