#include <fstream>
#include <iostream>
#include <sstream>
#include <stdlib.h>

#include "HelperFunctions.h"

namespace Charm {

TString get_run_number_string(bool is_mc) {
    if (is_mc) {
        return "EventInfo_RandomRunNumber";
    } else {
        return "EventInfo_runNumber";
    }
}

std::string basename(std::string filePath, bool withExtension, char seperator) {
    // Get last dot position
    std::size_t dotPos = filePath.rfind('.');
    std::size_t sepPos = filePath.rfind(seperator);

    if (sepPos != std::string::npos) {
        return filePath.substr(
            sepPos + 1,
            filePath.size() -
                (withExtension || dotPos != std::string::npos ? 1 : dotPos));
    }
    return "";
}

float get_cross_section(int id) {

    std::string data_dir(std::string(getenv("CHARMPP_BUILD_PATH")) + "/data");
    std::ifstream f(data_dir + "/cross_sections.txt");

    std::string line;

    while (getline(f, line)) {
        if (line.rfind("#", 0) == 0)
            continue;
        if (line.length() < 6)
            continue;

        std::istringstream ss(line);
        int dsid;
        std::string name;
        float xsec, kfac, eff, relunc;

        ss >> dsid >> name >> xsec >> kfac >> eff >> relunc;

        // std::cout << dsid << " " << name << " " << xsec << " " << kfac << " " << eff << " " << relunc << std::endl;

        if (dsid == id)
            return xsec*kfac*eff;

    }

    return 1;
}

} // namespace Charm