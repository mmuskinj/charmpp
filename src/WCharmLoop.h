#ifndef W_PLUS_CHARM_LOOP_H
#define W_PLUS_CHARM_LOOP_H

#include "EventLoopBase.h"

namespace Charm {

class WCharmLoop : public EventLoopBase {
  public:
    WCharmLoop(TString input_file, TString out_path, double init_sum_of_w, TString tree_name = "CharmAnalysis");

  private:
    virtual void read_meta_data() override;

    virtual void initialize() override;

    virtual int execute() override;

    // virtual void finalize() override;

  private:
    void initialize_histograms();
    void fill_histograms();

  private:
    void connect_branches();

  private:
    void construct_met();
    double m_met;
    double m_met_phi;
    double m_met_dphi;
    double m_met_mt;

    TLorentzVector m_lep;

  private:
    void get_electrons();
    void get_muons();
    bool get_jets();
    bool electron_is_trigger_matched(unsigned int i);
    bool muon_is_trigger_matched(unsigned int i);

  private:
    // runNumber of RandomRunNumber
    TString get_run_number_string();

    // isMC
    bool m_is_mc;

    // DSID
    int m_dataset_id;
    float m_cross_section;
    float m_lumi;

    std::vector<std::pair<TLorentzVector, float>> m_electrons;
    std::vector<std::pair<TLorentzVector, float>> m_muons;
    std::vector<std::pair<TLorentzVector, float>> m_jets;

  private:
    // branches
    UInt_t m_EventInfo_runNumber;
    Float_t m_EventInfo_correctedScaled_averageInteractionsPerCrossing;
    Float_t m_EventInfo_NOSYS_PileupWeight;
    Float_t m_EventInfo_jvt_effSF_NOSYS;
    Float_t m_EventInfo_fjvt_effSF_NOSYS;
    bool m_EventInfo_trigPassed_HLT_e24_lhmedium_L1EM20VH;
    bool m_EventInfo_trigPassed_HLT_e60_lhmedium;
    bool m_EventInfo_trigPassed_HLT_e120_lhloose;
    bool m_EventInfo_trigPassed_HLT_e26_lhtight_nod0_ivarloose;
    bool m_EventInfo_trigPassed_HLT_e60_lhmedium_nod0;
    bool m_EventInfo_trigPassed_HLT_e140_lhloose_nod0;
    bool m_EventInfo_trigPassed_HLT_mu20_iloose_L1MU15;
    bool m_EventInfo_trigPassed_HLT_mu26_ivarmedium;
    bool m_EventInfo_trigPassed_HLT_mu50;

    std::vector<Float_t> *m_EventInfo_mcEventWeights;

    // jets
    std::vector<Float_t> *m_AnalysisJetsFinal_NOSYS_m;
    std::vector<Float_t> *m_AnalysisJetsFinal_NOSYS_pt;
    std::vector<Float_t> *m_AnalysisJetsFinal_eta;
    std::vector<Float_t> *m_AnalysisJetsFinal_phi;
    std::vector<bool> *m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_60;
    std::vector<bool> *m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_70;
    std::vector<bool> *m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_77;
    std::vector<bool> *m_AnalysisJetsFinal_NOSYS_ftag_select_MV2c10_FixedCutBEff_85;

    // muons
    std::vector<Float_t> *m_AnalysisMuonsOR_NOSYS_pt;
    std::vector<Float_t> *m_AnalysisMuonsOR_eta;
    std::vector<Float_t> *m_AnalysisMuonsOR_phi;
    std::vector<Float_t> *m_AnalysisMuonsOR_charge;
    std::vector<Float_t> *m_AnalysisMuonsOR_muon_effSF_NOSYS;
    std::vector<Float_t> *m_AnalysisMuonsOR_muon_effSF_TTVA_NOSYS;
    std::vector<Float_t> *m_AnalysisMuonsOR_muon_effSF_Isol_FCTightTrackOnly_FixedRad_NOSYS;
    std::vector<Float_t> *m_AnalysisMuonsOR_muon_effSF_Isol_FCLoose_FixedRad_NOSYS;
    std::vector<Float_t> *m_AnalysisMuonsOR_muon_effSF_Isol_FCTight_FixedRad_NOSYS;
    std::vector<Float_t> *m_AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;
    std::vector<Float_t> *m_AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu26_ivarmedium_OR_HLT_mu50_NOSYS;
    std::vector<Float_t> *m_AnalysisMuonsOR_muon_effMC_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;
    std::vector<Float_t> *m_AnalysisMuonsOR_muon_effData_Trig_Medium_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_NOSYS;
    std::vector<bool> *m_AnalysisMuonsOR_NOSYS_isIsolated_FCTightTrackOnly_FixedRad;
    std::vector<bool> *m_AnalysisMuonsOR_NOSYS_isIsolated_FCLoose_FixedRad;
    std::vector<bool> *m_AnalysisMuonsOR_NOSYS_isIsolated_FCTight_FixedRad;
    std::vector<bool> *m_AnalysisMuonsOR_matched_HLT_mu20_iloose_L1MU15;
    std::vector<bool> *m_AnalysisMuonsOR_matched_HLT_mu26_ivarmedium;
    std::vector<bool> *m_AnalysisMuonsOR_matched_HLT_mu50;

    // electrons
    std::vector<Float_t> *m_AnalysisElectronsOR_NOSYS_pt;
    std::vector<Float_t> *m_AnalysisElectronsOR_eta;
    std::vector<Float_t> *m_AnalysisElectronsOR_phi;
    std::vector<Float_t> *m_AnalysisElectronsOR_charge;
    std::vector<Float_t> *m_AnalysisElectronsOR_effSF_NOSYS;
    std::vector<Float_t> *m_AnalysisElectronsOR_effSF_ID_Tight_NOSYS;
    std::vector<Float_t> *m_AnalysisElectronsOR_effSF_Isol_Tight_FCTight_NOSYS;
    std::vector<Float_t> *m_AnalysisElectronsOR_effSF_Trig_Tight_FCTight_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_NOSYS;
    std::vector<bool> *m_AnalysisElectronsOR_likelihood_Medium;
    std::vector<bool> *m_AnalysisElectronsOR_likelihood_Tight;
    std::vector<bool> *m_AnalysisElectronsOR_NOSYS_isIsolated_FCHighPtCaloOnly;
    std::vector<bool> *m_AnalysisElectronsOR_NOSYS_isIsolated_FCLoose;
    std::vector<bool> *m_AnalysisElectronsOR_NOSYS_isIsolated_FCTight;
    std::vector<bool> *m_AnalysisElectronsOR_NOSYS_isIsolated_Gradient;
    std::vector<bool> *m_AnalysisElectronsOR_matched_HLT_e24_lhmedium_L1EM20VH;
    std::vector<bool> *m_AnalysisElectronsOR_matched_HLT_e60_lhmedium;
    std::vector<bool> *m_AnalysisElectronsOR_matched_HLT_e120_lhloose;
    std::vector<bool> *m_AnalysisElectronsOR_matched_HLT_e26_lhtight_nod0_ivarloose;
    std::vector<bool> *m_AnalysisElectronsOR_matched_HLT_e60_lhmedium_nod0;
    std::vector<bool> *m_AnalysisElectronsOR_matched_HLT_e140_lhloose_nod0;

    // MET
    std::vector<std::string> *m_AnalysisMET_name;
    std::vector<double> *m_AnalysisMET_NOSYS_mpx;
    std::vector<double> *m_AnalysisMET_NOSYS_mpy;
    std::vector<double> *m_AnalysisMET_NOSYS_sumet;
    std::vector<float> *m_AnalysisMET_NOSYS_significance;
};

} // namespace Charm

#endif // W_PLUS_CHARM_LOOP_H
