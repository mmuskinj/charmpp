#ifndef EVENT_LOOP_BASE_H
#define EVENT_LOOP_BASE_H

#include "TFile.h"
#include "TString.h"
#include "TTree.h"
#include "TLorentzVector.h"

#include "Definitions.h"

namespace Charm {

class EventLoopBase {
  public:
    EventLoopBase(TString input_file, TString out_path, double init_sum_of_w, TString tree_name);

    // execute event loop
    void run();

  private:
    virtual void read_meta_data();

    virtual void initialize();

    virtual int execute();

    virtual void finalize();

    // TTree name
    TString m_tree_name;

    // out name
    TString m_output_file_name;

  protected:
    // input root file
    TFile *m_input_file;

    // TTree
    TTree *m_tree;

    // Add branch
    void add_branch(TString name);

    // List of branches
    std::vector<TBranch *> m_branches;

    // number of events
    long long m_current_event;
    long long m_num_events;
    TString m_out_path;
    double m_init_sum_of_w;

    // histograms
    std::map<TString, TH1 *> m_histograms;

    // channel
    TString m_channel;

    // event weight
    float m_event_weight;

    void fill_histogram(TString name, float val);

    void add_histogram(TString name, int n, float xmin, float xmax);

};

} // namespace Charm

#endif // EVENT_LOOP_BASE_H
